/**
 * These are similar to the MLMC tests for the MCQMC06 paper
 * using a Milstein discretisation with 2^l timesteps on level l
 *
 * The figures are slightly different due to
 * -- change in MSE split
 * -- change in cost calculation
 * -- different random number generation
 * -- switch to S_0=100
 *
 * The code in this version demonstrates in particular how to 
 * achieve excellent performance using OpenMP multi-threaded
 * parallisation.  To achieve the best performance it is very
 * important that
 * -- each thread has its own copy of the random number generator
 * -- each thread generates random numbers in chunks which are
 *    big enough to achieve good vector efficiency, but small
 *    enough to remain in L2 cache
 */

#include "mlmc_test_100.cpp"  // master file for 100 iid tests
#include "mlmc_test.cpp"      // master file for convergence tests

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mkl.h>
#include <mkl_vsl.h>
#include <memory.h>

#ifdef _OPENMP
    #include <omp.h>
#else
    #include <time.h>
#endif

/* define whether using float or double */
#define PREC 1
#if PREC == 1
  #define REAL float
#else
  #define REAL double
#endif

//
// global variables
//

int option;         // parameter to specify the financial option
// GBM model parameters
const REAL r   = 0.05;      // drift
const REAL sig = 0.2;       // local volatility
const REAL K   = 100.0;     // strike price & initial value
const REAL B   = 0.85*K;    // barrier value
const REAL S0  = 100.0;     // initial value stock price
const REAL T   = 1.0;       // time interval

//
// function declarations
//

void mcqmc06_omp_l(int l, int N, double *sums);
void mcqmc06_vec_l(int l, int N, double *sums);
// Low-level routine returns sums containing N samples of the
// level l Milstein discretisation of GBM for a given option

double ncf(double x);
float ncff(float x);
// Returns the Normal CDF evaluated at x

//
// RNG routines
//

/* each OpenMP thread has its own VSL RNG and storage */
#define RV_BYTES 65536
VSLStreamStatePtr stream;
REAL  *normals,       *exponentials;
int    normals_count,  exponentials_count;
#pragma omp threadprivate(stream, normals, normals_count, \
                        exponentials, exponentials_count)

void rng_initialisation() {
#ifdef _OPENMP
  int tid = omp_get_thread_num();
#else
  int tid = 0;
#endif
  vslNewStream(&stream, VSL_BRNG_MRG32K3A, 1337);
  long long skip = ((long long) (tid+1)) << 48;
  vslSkipAheadStream(stream, skip);
  normals      = (REAL *)malloc(RV_BYTES);
  exponentials = (REAL *)malloc(RV_BYTES);
  normals_count      = 0;  // this means there are no random
  exponentials_count = 0;  // numbers in the arrays currently
}

void rng_termination() {
  vslDeleteStream(&stream);
  free(normals);
  free(exponentials);
}

REAL next_normal() {
  if (normals_count == 0) {
    int N = RV_BYTES / sizeof(REAL);
#if PREC == 1
    vsRngGaussian(VSL_RNG_METHOD_GAUSSIAN_BOXMULLER2,
                  stream, N, normals, 0.0f, 1.0f);
#else
    vdRngGaussian(VSL_RNG_METHOD_GAUSSIAN_BOXMULLER2,
                  stream, N, normals, 0.0, 1.0);
#endif
    normals_count = N;
  }
  return normals[--normals_count];
}

REAL next_exponential() {
  if (exponentials_count == 0) {
    int N = RV_BYTES / sizeof(REAL);
#if PREC == 1
    vsRngExponential(VSL_RNG_METHOD_EXPONENTIAL_ICDF_ACCURATE,
                     stream, N, exponentials, 0.0f, 1.0f);
#else
    vdRngExponential(VSL_RNG_METHOD_EXPONENTIAL_ICDF_ACCURATE,
                     stream, N, exponentials, 0.0, 1.0);
#endif
    exponentials_count = N;
  }
  return exponentials[--exponentials_count];
}

// main code

int main(int argc, char **argv) {

  int N0 = 200;   // initial samples on each level
  int Lmin = 2;   // minimum refinement level
  int Lmax = 10;  // maximum refinement level

  int N, L;
  float Eps[11];
  char filename[32];
  FILE *fp;

#ifdef _OPENMP
  double wtime = omp_get_wtime();
#else
  struct timespec wtime1, wtime2;
  clock_gettime(CLOCK_MONOTONIC, &wtime1);
#endif

//
// loop over different payoffs
//

  for (option = 1; option < 6; option++) {

    // initialise generator and storage for each thread
#pragma omp parallel
    rng_initialisation();

    sprintf(filename, "mcqmc06_omp_%d.txt", option);
    fp = fopen(filename, "w");

    if (option == 1) {
      printf("\n ---- option %d: European call ----\n", option);
      N      = 20000;    // samples for convergence tests
      L      = 8;        // levels for convergence tests
      float Eps2[] = { 0.005, 0.01, 0.02, 0.05, 0.1, 0.0 };
      memcpy(Eps, Eps2, sizeof(Eps2));
    }
    else if (option == 2) {
      printf("\n ---- option %d: Asian call ----\n", option);
      N      = 20000;    // samples for convergence tests
      L      = 8;        // levels for convergence tests
      float Eps2[] = { 0.005, 0.01, 0.02, 0.05, 0.1, 0.0 };
      memcpy(Eps, Eps2, sizeof(Eps2));
    }
    else if (option == 3) {
      printf("\n ---- option %d: lookback call ----\n", option);
      N      = 20000;    // samples for convergence tests
      L      = 10;       // levels for convergence tests
      float Eps2[] = { 0.005, 0.01, 0.02, 0.05, 0.1, 0.0 };
      memcpy(Eps, Eps2, sizeof(Eps2));
    }
    else if (option == 4) {
      printf("\n ---- option %d: digital call ----\n", option);
      N      = 200000;   // samples for convergence tests
      L      = 8;        // levels for convergence tests
      float Eps2[] = { 0.01, 0.02, 0.05, 0.1, 0.2, 0.0 };
      memcpy(Eps, Eps2, sizeof(Eps2));
    }
    else if (option == 5) {
      printf("\n ---- option %d: barrier call ----\n", option);
      N      = 200000;   // samples for convergence tests
      L      = 8;        // levels for convergence tests
      float Eps2[] = { 0.005, 0.01, 0.02, 0.05, 0.1, 0.0 };
      memcpy(Eps, Eps2, sizeof(Eps2));
    }

    mlmc_test(mcqmc06_omp_l, N,L, N0,Eps, Lmin,Lmax, fp);

    fclose(fp);

    // print out time taken
#ifdef _OPENMP
    printf("\n execution time = %f s\n", omp_get_wtime() - wtime);
    wtime = omp_get_wtime();
#else
    clock_gettime(CLOCK_MONOTONIC, &wtime2);
    printf("\n execution time = %f s\n", (double)(wtime2.tv_sec + 1.0e-9*wtime2.tv_nsec) - 
            (double)(wtime1.tv_sec + 1.0e-9*wtime1.tv_nsec));
    wtime1 = wtime2;
#endif

    // delete generator and storage
#pragma omp parallel
    rng_termination();

    //
    // print exact analytic value
    //

    float d1, d2, val, k, d3, d4;

    float sig2 = sig*sig;

    if (option == 1) {
      d1  = ( logf(S0/K) + (r+0.5f*sig2)*T) / (sig*sqrtf(T));
      d2  = ( logf(S0/K) + (r-0.5f*sig2)*T) / (sig*sqrtf(T));
      val = S0*ncff(d1) - K*expf(-r*T)*ncff(d2);
    }
    else if (option == 2) {
      val = nanf("");
    }
    else if (option == 3) {
      k   = 0.5f*sig2/r;
      d1  = (r+0.5f*sig2)*T / (sig*sqrtf(T));
      d2  = (r-0.5f*sig2)*T / (sig*sqrtf(T));
      val = S0*( ncff(d1) - ncff(-d1)*k - exp(-r*T)*(ncff(d2) - ncff(d2)*k) );
    }
    else if (option == 4) {
      d2  = ( logf(S0/K) + (r-0.5f*sig2)*T) / (sig*sqrtf(T));
      val = K*expf(-r*T)*ncff(d2);
    }
    else {
      k   = 0.5f*sig2/r;
      if ( K > B ) {
        d1  = ( logf(S0/K) + (r+0.5f*sig2)*T) / (sig*sqrtf(T));
        d2  = ( logf(S0/K) + (r-0.5f*sig2)*T) / (sig*sqrtf(T));
        d3  = ( logf(B/S0) + logf(B/K) + (r+0.5f*sig2)*T) / (sig*sqrtf(T));
        d4  = ( logf(B/S0) + logf(B/K) + (r-0.5f*sig2)*T) / (sig*sqrtf(T));
      }
      else {
        d1  = ( logf(S0/B) + (r+0.5f*sig2)*T) / (sig*sqrtf(T));
        d2  = ( logf(S0/B) + (r-0.5f*sig2)*T) / (sig*sqrtf(T));
        d3  = ( logf(B/S0) + (r+0.5f*sig2)*T) / (sig*sqrtf(T));
        d4  = ( logf(B/S0) + (r-0.5f*sig2)*T) / (sig*sqrtf(T));
      }
      val =  S0*ncff(d1) - K*expf(-r*T)*ncff(d2) \
    - powf(S0/B, 1.0f-1.0f/k)*( (B*B)/(S0)*ncff(d3) - K*expf(-r*T)*ncff(d4) ) ;
    }

    //
    // now do 100 MLMC calcs
    //

    // initialise generator and storage for each thread
#pragma omp parallel
    rng_initialisation();

    sprintf(filename,"mcqmc06_omp_%d_100.txt",option);
    fp = fopen(filename,"w");

    mlmc_test_100(mcqmc06_omp_l, val, N0,Eps,Lmin,Lmax, fp);

    fclose(fp);

    // print out time taken
#ifdef _OPENMP
    printf(" execution time = %f s\n", omp_get_wtime() - wtime);
    wtime = omp_get_wtime();
#else
    clock_gettime(CLOCK_MONOTONIC, &wtime2);
    printf(" execution time = %f s\n", (double)(wtime2.tv_sec + 1.0e-9*wtime2.tv_nsec) - 
            (double)(wtime1.tv_sec + 1.0e-9*wtime1.tv_nsec));
    wtime1 = wtime2;
#endif

    // delete generator and storage
#pragma omp parallel
    rng_termination();

  }
}

/*-------------------------------------------------------
 *
 * level l multiple thread OpenMP estimator
 *
 */

void mcqmc06_omp_l(int l, int N, double *sums) {

  for (int k = 0; k < 7; k++) sums[k] = 0.0;

#pragma omp parallel shared(sums)
  {
    double sum2[7]={};

#ifdef _OPENMP
    int num_t = omp_get_num_threads();
    int tid   = omp_get_thread_num();
#else
    int num_t = 1;
    int tid   = 0;
#endif
    // Number of samples on current thread
    int N2    = ((tid+1)*N)/num_t - (tid*N)/num_t;
    mcqmc06_vec_l(l, N2, sum2);

#pragma omp critical
    for (int k = 0; k < 7; k++) sums[k] += sum2[k];
  }
}

/*-------------------------------------------------------
 *
 * level l single thread estimator
 *
 */

void mcqmc06_vec_l(int l, int N, double *sums) {

  int   nf, nc;
  REAL  hf, hc, X0, Xf, Xc, Af, Ac, Mf, Mc, Bf, Bc,
        Xf0, Xc0, Xc1, vf, vc, dWc, ddW, Pf, Pc, dP,
        dW, dI, L, dWf[2], dIf[2], Lf[2];

  // Fix refinement factor to 2
  nf = 1 << l;
  nc = nf/2;

  hf = T / ((float) nf);
  hc = T / ((float) nc);

  for (int k = 0; k < 7; k++) sums[k] = 0.0;

  /* branch depending on whether or not level 0 */
  if (l == 0) {

    for (int n2 = 0; n2 < N; n2++) {
      Xf0 = S0;
      Xf  = Xf0;
      vf  = sig*Xf;

      dW = sqrt(hf)*next_normal();
      Xf = Xf + r*Xf*hf + sig*Xf*dW + 0.5f*sig*sig*Xf*(dW*dW-hf);
      if (option == 1) {
        Pf = fmax(0.0f, Xf-K);
      }
      else if (option == 2) {
        dI = hf*sqrt(hf/12.0f)*next_normal();
        Af = 0.5f*hf*Xf0 + 0.5f*hf*Xf + vf*dI;
        Pf = fmax(0.0f, Af-K);
      }
      else if (option == 3) {
        L  = next_exponential();
        Mf = 0.5f*(Xf0+Xf-sqrt((Xf-Xf0)*(Xf-Xf0)+2.0f*hf*vf*vf*L));
        Pf = Xf - Mf;
      }
      else if (option == 4) {
        Pf = K*ncf((Xf0+r*Xf0*hf-K)/(sig*Xf0*sqrt(hf)));
      }
      else if (option == 5) {
        Bf = 1.0f-exp(-2.0f*fmax(0.0f, (Xf0-B)*(Xf-B)/(hf*vf*vf)));
        Pf = Bf*fmax(0.0f, Xf-K);
      }

      Pf = exp(-r*T) * Pf;

      sums[0] += nf;     // add number of timesteps as cost
      sums[1] += Pf;
      sums[2] += Pf*Pf;
      sums[3] += Pf*Pf*Pf;
      sums[4] += Pf*Pf*Pf*Pf;
      sums[5] += Pf;
      sums[6] += Pf*Pf;
    }
  }

  else {

    for (int n2 = 0; n2 < N; n2++) {
      Xf = S0;
      Xc = Xf;

      Af = 0.5f*hf*Xf;
      Ac = 0.5f*hc*Xc;

      Mf = Xf;
      Mc = Xc;

      Bf = 1.0f;
      Bc = 1.0f;

      for (int n = 0; n < nc; n++) {
        for (int m = 0; m < 2; m++) {
          dWf[m] = sqrt(hf)*next_normal();
          Xf0 = Xf;
          Xf  = Xf + r*Xf*hf + sig*Xf*dWf[m]
                   + 0.5f*sig*sig*Xf*(dWf[m]*dWf[m]-hf);

          vf  = sig*Xf0;
          if (option == 2) {
            dIf[m] = hf*sqrt(hf/12.0f)*next_normal();
            Af = Af + hf*Xf + vf*dIf[m];
          }
          if (option == 3) {
            Lf[m] = next_exponential();
            Mf = fmin(Mf,
                  0.5f*(Xf0+Xf-sqrt((Xf-Xf0)*(Xf-Xf0)+2.0f*hf*vf*vf*Lf[m])));
          }
          if (option == 5) {
            Bf = Bf*(1.0f-exp(-2.0f*fmax(0.0f, (Xf0-B)*(Xf-B)/(hf*vf*vf))));
          }
        }

        dWc = dWf[0] + dWf[1];
        Xc0 = Xc;
        Xc  = Xc + r*Xc*hc + sig*Xc*dWc + 0.5f*sig*sig*Xc*(dWc*dWc-hc);

        vc  = sig*Xc0;
        if (option == 2) {
          ddW = dWf[0] - dWf[1];
          Ac  = Ac + hc*Xc + vc*(dIf[0]+dIf[1] + 0.25f*hc*ddW);
        }
        if (option == 3) {
          ddW = dWf[0] - dWf[1];
          Xc1 = 0.5f*(Xc0 + Xc + vc*ddW);
          Mc  = fmin(Mc,
              0.5f*(Xc0+Xc1-sqrt((Xc1-Xc0)*(Xc1-Xc0)+2.0f*hf*vc*vc*Lf[0])));
          Mc  = fmin(Mc,
              0.5f*(Xc1+Xc -sqrt((Xc -Xc1)*(Xc -Xc1)+2.0f*hf*vc*vc*Lf[1])));
        }
        if (option == 5) {
          ddW = dWf[0] - dWf[1];
          Xc1 = 0.5f*(Xc0 + Xc + vc*ddW);
          Bc  = Bc *(1.0f-exp(-2.0f*fmax(0.0f, (Xc0-B)*(Xc1-B)/(hf*vc*vc))));
          Bc  = Bc *(1.0f-exp(-2.0f*fmax(0.0f, (Xc1-B)*(Xc -B)/(hf*vc*vc))));
        }
      }

      if (option == 1) {
        Pf = fmax(0.0f, Xf-K);
        Pc = fmax(0.0f, Xc-K);
      }
      else if (option == 2) {
        Af = Af - 0.5f*hf*Xf;
        Ac = Ac - 0.5f*hc*Xc;
        Pf = fmax(0.0f, Af-K);
        Pc = fmax(0.0f, Ac-K);
      }
      else if (option == 3) {
        Pf = Xf - Mf;
        Pc = Xc - Mc;
      }
      else if (option == 4) {
        Pf = K*ncf((Xf0+r*Xf0*hf-K)/(sig*Xf0*sqrt(hf)));
        Pc = K*ncf((Xc0+r*Xc0*hc+sig*Xc0*dWf[0]-K)/(sig*Xc0*sqrt(hf)));
      }
      else if (option == 5) {
        Pf = Bf*fmax(0.0f, Xf-K);
        Pc = Bc*fmax(0.0f, Xc-K);
      }

      Pf = exp(-r*T) * Pf;
      Pc = exp(-r*T) * Pc;
      dP = Pf-Pc;

      sums[0] += nf;     // add number of timesteps as cost
      sums[1] += dP;
      sums[2] += dP*dP;
      sums[3] += dP*dP*dP;
      sums[4] += dP*dP*dP*dP;
      sums[5] += Pf;
      sums[6] += Pf*Pf;
    }
  }
}

//
// Normal CDF function
//

float ncff(float x) {
  return 0.5f+0.5f*erff(sqrtf(0.5f)*x);
}

double ncf(double x) {
  return 0.5+0.5*erf(sqrt(0.5)*x);
}
