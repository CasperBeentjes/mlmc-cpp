/**
 * These are similar to the MLMC tests for the original
 * 2008 Operations Research paper, using an Euler-Maruyama
 * discretisation with 4^l timesteps on level l.
 *
 * The differences are:
 * -- the plots do not have the extrapolation results
 * -- the top two plots are log_2 rather than log_4
 * -- the new MLMC driver is a little different
 * -- switch to X_0=100 instead of X_0=1
 *
 *   The code in this version demonstrates in particular how to
 *   achieve excellent performance using OpenMP multi-threaded
 *   parallelisation. To achieve the best performance it is very
 *   important that
 *   -- each thread has its own copy of the random number generator
 *   -- each thread generates random numbers in chunks which are
 *      big enough to achieve good vector efficiency, but small
 *      enough to remain in L2 cache
 */

#include "mlmc_test_100.cpp"  // master file for 100 iid tests
#include "mlmc_test.cpp"      // master file for convergence tests

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mkl.h>
#include <mkl_vsl.h>
#include <memory.h>

#ifdef _OPENMP
    #include <omp.h>
#else
    #include <time.h>
#endif

/* define whether using float or double */
#define PREC 1
#if PREC == 1
  #define REAL float
#else
  #define REAL double
#endif

//
// global variables
//

int option;  // parameter to specify the financial option

// GBM model parameters
const REAL r   = 0.05;      // drift
const REAL sig = 0.2;       // local volatility
const REAL K   = 100.0;     // strike price & initial value
const REAL S0  = 100.0;     // initial value stock price
const REAL T   = 1.0;       // time interval
// Heston model additional parameters
const REAL lambda = 5.0;    // rate of reversal of instantaneous variance
const REAL xsi    = 0.25;   // volatility of volatility
const REAL V0     = 0.04;   // initial value instantaneous variance
const REAL rho    = -0.5;   // correlation between dW_S and dW_V

//
// function declarations
//

void opre_omp_l(int l, int N, double *sums);
void opre_vec_l(int l, int N, double *sums);
// Low-level routine returns sums containing N samples of the
// level l Euler-Maruyama discretisation of underlying model
// for a given option

void mu_Heston(double *x, double h, double ( & )[2]);
void mu_Hestonf(float *x, float h, float ( & )[2]);
// Returns the drift in the Heston model
void sigdW_Heston(double *x, double *dW, double h, double ( & )[2]);
void sigdW_Hestonf(float *x, float *dW, float h, float ( & )[2]);
// Returns the volatility in the Heston model

double ncf(double x);
float ncff(float x);
// Returns the Normal CDF evaluated at x

//
// RNG routines
//

/* each OpenMP thread has its own VSL RNG and storage */
#define RV_BYTES 65536
VSLStreamStatePtr stream;
REAL  *normals;
int    normals_count;
#pragma omp threadprivate(stream, normals, normals_count)

void rng_initialisation() {
#ifdef _OPENMP
  int tid = omp_get_thread_num();
#else
  int tid = 0;
#endif
  vslNewStream(&stream, VSL_BRNG_MRG32K3A, 1337);
  long long skip = ((long long) (tid+1)) << 48;
  vslSkipAheadStream(stream, skip);
  normals      = (REAL *)malloc(RV_BYTES);
  normals_count      = 0;  // this means there are no random
}

void rng_termination() {
  vslDeleteStream(&stream);
  free(normals);
}

REAL next_normal() {
  if (normals_count == 0) {
    int N = RV_BYTES / sizeof(REAL);
#if PREC == 1
    vsRngGaussian(VSL_RNG_METHOD_GAUSSIAN_BOXMULLER2,
                  stream, N, normals, 0.0f, 1.0f);
#else
    vdRngGaussian(VSL_RNG_METHOD_GAUSSIAN_BOXMULLER2,
                  stream, N, normals, 0.0, 1.0);
#endif
    normals_count = N;
  }
  return normals[--normals_count];
}

// main code

int main(int argc, char **argv) {

  int N0 = 1000;  // initial samples on each level
  int Lmin = 2;   // minimum refinement level
  int Lmax = 6;   // maximum refinement level

  int N, L;
  float Eps[11];
  char filename[32];
  FILE *fp;

#ifdef _OPENMP
  double wtime = omp_get_wtime();
#else
  struct timespec wtime1, wtime2;
  clock_gettime(CLOCK_MONOTONIC, &wtime1);
#endif

//
// loop over different payoffs
//

  for (option = 1; option < 6; option++) {

    // initialise generator and storage for each thread
#pragma omp parallel
    rng_initialisation();

    if (option < 5) {
      sprintf(filename, "opre_omp_gbm%d.txt", option);
    }
    else {
      sprintf(filename, "opre_omp_heston.txt");
    }
    fp = fopen(filename, "w");

    if (option == 1) {
      printf("\n ---- European call ----\n");
      N      = 2000000;  // samples for convergence tests
      L      = 5;        // levels for convergence tests
      float Eps2[] = { 0.005, 0.01, 0.02, 0.05, 0.1, 0.0 };
      memcpy(Eps, Eps2, sizeof(Eps2));
    }
    else if (option == 2) {
      printf("\n ---- Asian call ----\n");
      N      = 2000000;  // samples for convergence tests
      L      = 5;        // levels for convergence tests
      float Eps2[] = { 0.005, 0.01, 0.02, 0.05, 0.1, 0.0 };
      memcpy(Eps, Eps2, sizeof(Eps2));
    }
    else if (option == 3) {
      printf("\n ---- lookback call ----\n");
      N      = 2000000;  // samples for convergence tests
      L      = 5;        // levels for convergence tests
      float Eps2[] = { 0.01, 0.02, 0.05, 0.1, 0.2, 0.0 };
      memcpy(Eps, Eps2, sizeof(Eps2));
    }
    else if (option == 4) {
      printf("\n ---- digital call ----\n");
      N      = 2000000;  // samples for convergence tests
      L      = 5;        // levels for convergence tests
      float Eps2[] = { 0.02, 0.05, 0.1, 0.2, 0.5, 0.0 };
      memcpy(Eps, Eps2, sizeof(Eps2));
    }
    else if (option == 5) {
      printf("\n ---- Heston model ----\n");
      N      = 2000000;  // samples for convergence tests
      L      = 5;        // levels for convergence tests
      float Eps2[] = { 0.005, 0.01, 0.02, 0.05, 0.1, 0.0 };
      memcpy(Eps, Eps2, sizeof(Eps2));
    }

    mlmc_test(opre_omp_l, N,L, N0,Eps, Lmin,Lmax, fp);

    fclose(fp);

    // print out time taken
#ifdef _OPENMP
    printf(" execution time = %f s\n", omp_get_wtime() - wtime);
    wtime = omp_get_wtime();
#else
    clock_gettime(CLOCK_MONOTONIC, &wtime2);
    printf(" execution time = %f s\n", (double)(wtime2.tv_sec + 1.0e-9*wtime2.tv_nsec) -
            (double)(wtime1.tv_sec + 1.0e-9*wtime1.tv_nsec));
    wtime1 = wtime2;
#endif

    // delete generator and storage
#pragma omp parallel
    rng_termination();

    //
    // print exact analytic value
    //

    float sig2, d1, d2, val, k;

    sig2 = sig*sig;

    d1  = ( logf(S0/K) + (r+0.5f*sig2)*T) / (sig*sqrtf(T));
    d2  = ( logf(S0/K) + (r-0.5f*sig2)*T) / (sig*sqrtf(T));

    if (option == 1) {
      val = S0*ncff(d1) - K*expf(-r*T)*ncff(d2);
    }
    else if (option == 3) {
      k   = 0.5f*sig2/r;
      d1  = (r+0.5f*sig2)*T / (sig*sqrtf(T));
      d2  = (r-0.5f*sig2)*T / (sig*sqrtf(T));
      val = S0*( ncff(d1) - ncff(-d1)*k - exp(-r*T)*(ncff(d2) - ncff(d2)*k) );
    }
    else if (option == 4) {
      val = K*expf(-r*T)*ncff(d2);
    }
    else {
      val = nanf("");
    }

    //
    // now do 100 MLMC calcs
    //

    // initialise generator and storage for each thread
#pragma omp parallel
    rng_initialisation();

    if (option < 5) {
      sprintf(filename, "opre_omp_gbm%d_100.txt", option);
    }
    else {
      sprintf(filename, "opre_omp_heston_100.txt");
    }
    fp = fopen(filename,"w");

    mlmc_test_100(opre_omp_l, val, N0,Eps,Lmin,Lmax, fp);

    fclose(fp);

    // print out time taken
#ifdef _OPENMP
    printf(" execution time = %f s\n", omp_get_wtime() - wtime);
#else
    clock_gettime(CLOCK_MONOTONIC, &wtime2);
    printf(" execution time = %f s\n", (double)(wtime2.tv_sec + 1.0e-9*wtime2.tv_nsec) -
            (double)(wtime1.tv_sec + 1.0e-9*wtime1.tv_nsec));
#endif

    // delete generator and storage
#pragma omp parallel
    rng_termination();

  }
}

/*-------------------------------------------------------
 *
 * level l multiple thread OpenMP estimator
 *
 */

void opre_omp_l(int l, int N, double *sums) {

  for (int k = 0; k < 7; k++) sums[k] = 0.0;

#pragma omp parallel shared(sums)
  {
    double sum2[7]={};

#ifdef _OPENMP
    int num_t = omp_get_num_threads();
    int tid   = omp_get_thread_num();
#else
    int num_t = 1;
    int tid   = 0;
#endif
    // Number of samples on current thread
    int N2    = ((tid+1)*N)/num_t - (tid*N)/num_t;
    opre_vec_l(l, N2, sum2);

#pragma omp critical
    for (int k = 0; k < 7; k++) sums[k] += sum2[k];
  }
}

/*-------------------------------------------------------
 *
 * level l single thread estimator
 *
 */

void opre_vec_l(int l, int N, double *sums) {

  int   nf, nc;
  REAL hf, hc, Af, Ac, Mf, Mc, Pf, Pc, dP;
  REAL X0[2], Xf[2], Xc[2], dWc[2], dWf[2];

  int M = 4;  // discretisation refinement parameter

  nf = (int) powf((float) M, (float) l);
  nc = nf/M;

  hf = T / ((float) nf);
  hc = T / ((float) nc);

  for (int k = 0; k < 7; k++) sums[k] = 0.0;

  //
  // GBM model
  //
  if (option < 5) {
    /* branch depending on whether or not level 0 */
    if (l == 0) {

      for (int n2 = 0; n2 < N; n2++) {
        X0[0] = S0;

        Xf[0] = X0[0];
        Xc[0] = Xf[0];

        Af = 0.5*hf*Xf[0];
        Ac = 0.5*hc*Xc[0];

        Mf = Xf[0];
        Mc = Xc[0];

        dWf[0] = sqrt(hf) * next_normal();

        Xf[0] = Xf[0] + r*Xf[0]*hf + sig*Xf[0]*dWf[0];
        Af    = Af + 0.5*hf*Xf[0];
        Mf    = fminf(Mf, Xf[0]);

        if (option == 1) {
          Pf = fmaxf(0.0, Xf[0]-K);
          Pc = fmaxf(0.0, Xc[0]-K);
        }
        else if (option == 2) {
          Pf = fmaxf(0.0, Af-K);
          Pc = fmaxf(0.0, Ac-K);
        }
        else if (option == 3) {
          REAL beta = 0.5826;
          Pf = Xf[0] - Mf*(1-beta*sig*sqrt(hf));
          Pc = Xc[0] - Mc*(1-beta*sig*sqrt(hc));
        }
        else if (option == 4) {
          Pf = K * 0.5*(copysign(1.0, Xf[0]-K)+1);
          Pc = K * 0.5*(copysign(1.0, Xc[0]-K)+1);
        }

        Pf = exp(-r*T) * Pf;

        sums[0] += nf;     // add number of timesteps as cost
        sums[1] += Pf;
        sums[2] += Pf*Pf;
        sums[3] += Pf*Pf*Pf;
        sums[4] += Pf*Pf*Pf*Pf;
        sums[5] += Pf;
        sums[6] += Pf*Pf;
      }
    }
    else {

      for (int n2 = 0; n2 < N; n2++) {
        X0[0] = S0;

        Xf[0] = X0[0];
        Xc[0] = Xf[0];

        Af = 0.5*hf*Xf[0];
        Ac = 0.5*hc*Xc[0];

        Mf = Xf[0];
        Mc = Xc[0];

        for (int n = 0; n < nc; n++) {
          dWc[0] = 0.0f;
          for (int m = 0; m < M; m++) {
            dWf[0]  = sqrt(hf) * next_normal();
            dWc[0] += dWf[0];

            Xf[0] = Xf[0] + r*Xf[0]*hf + sig*Xf[0]*dWf[0];
            Af    = Af + hf*Xf[0];
            Mf    = fminf(Mf, Xf[0]);
          }

          Xc[0] = Xc[0] + r*Xc[0]*hc + sig*Xc[0]*dWc[0];
          Ac    = Ac + hc*Xc[0];
          Mc    = fminf(Mc, Xc[0]);
        }
        Af = Af - 0.5f*hf*Xf[0];
        Ac = Ac - 0.5f*hc*Xc[0];

        if (option == 1) {
          Pf = fmaxf(0.0, Xf[0]-K);
          Pc = fmaxf(0.0, Xc[0]-K);
        }
        else if (option == 2) {
          Pf = fmaxf(0.0, Af-K);
          Pc = fmaxf(0.0, Ac-K);
        }
        else if (option == 3) {
          REAL beta = 0.5826;
          Pf = Xf[0] - Mf*(1-beta*sig*sqrt(hf));
          Pc = Xc[0] - Mc*(1-beta*sig*sqrt(hc));
        }
        else if (option == 4) {
          Pf = K * 0.5*(copysign(1.0, Xf[0]-K)+1);
          Pc = K * 0.5*(copysign(1.0, Xc[0]-K)+1);
        }

        Pf = exp(-r*T) * Pf;
        Pc = exp(-r*T) * Pc;
        dP = Pf-Pc;

        sums[0] += nf;     // add number of timesteps as cost
        sums[1] += dP;
        sums[2] += dP*dP;
        sums[3] += dP*dP*dP;
        sums[4] += dP*dP*dP*dP;
        sums[5] += Pf;
        sums[6] += Pf*Pf;
      }
    }
  }
  //
  // Heston model
  //
  else {
    /* branch depending on whether or not level 0 */
    if (l == 0) {

      for (int n2 = 0; n2 < N; n2++) {
        REAL mu[2], sigdW[2];

        X0[0] = S0;
        X0[1] = V0;

        Xf[0] = X0[0];
        Xf[1] = X0[1];
        Xc[0] = Xf[0];
        Xc[1] = Xf[1];

        dWf[0] = sqrt(hf) * next_normal();
        dWf[1] = sqrt(hf) * next_normal();

#if PREC == 1
        mu_Hestonf(Xf, hf, mu);
        sigdW_Hestonf(Xf, dWf, hf, sigdW);
#else
        mu_Heston(Xf, hf, mu);
        sigdW_Heston(Xf, dWf, hf, sigdW);
#endif

        Xf[0]  = Xf[0] + mu[0]*hf + sigdW[0];
        Xf[1]  = Xf[1] + mu[1]*hf + sigdW[1];

        Pf  = exp(-r*T)*fmax(0.0f, Xf[0]-K);
        dP  = Pf;

        sums[0] += nf;     // add number of timesteps as cost
        sums[1] += dP;
        sums[2] += dP*dP;
        sums[3] += dP*dP*dP;
        sums[4] += dP*dP*dP*dP;
        sums[5] += Pf;
        sums[6] += Pf*Pf;
      }
    }
    else {

      for (int n2 = 0; n2 < N; n2++) {
        REAL mu[2], sigdW[2];

        X0[0] = S0;
        X0[1] = V0;

        Xf[0] = X0[0];
        Xf[1] = X0[1];
        Xc[0] = Xf[0];
        Xc[1] = Xf[1];

        for (int n = 0; n < nc; n++) {
          dWc[0]  = 0.0;
          dWc[1]  = 0.0;
          for (int m = 0; m < M; m++) {
            dWf[0]  = sqrt(hf) * next_normal();
            dWf[1]  = sqrt(hf) * next_normal();
            dWc[0] += dWf[0];
            dWc[1] += dWf[1];

#if PREC == 1
            mu_Hestonf(Xf, hf, mu);
            sigdW_Hestonf(Xf, dWf, hf, sigdW);
#else
            mu_Heston(Xf, hf, mu);
            sigdW_Heston(Xf, dWf, hf, sigdW);
#endif

            Xf[0]  = Xf[0] + mu[0]*hf + sigdW[0];
            Xf[1]  = Xf[1] + mu[1]*hf + sigdW[1];
          }
#if PREC == 1
            mu_Hestonf(Xc, hc, mu);
            sigdW_Hestonf(Xc, dWc, hc, sigdW);
#else
            mu_Heston(Xc, hc, mu);
            sigdW_Heston(Xc, dWc, hc, sigdW);
#endif

          Xc[0]  = Xc[0] + mu[0]*hc + sigdW[0];
          Xc[1]  = Xc[1] + mu[1]*hc + sigdW[1];
        }

        Pf  = fmax(0.0, Xf[0]-K);
        Pc  = fmax(0.0, Xc[0]-K);

        dP  = exp(-r*T)*(Pf-Pc);
        Pf  = exp(-r*T)*Pf;

        sums[0] += nf;     // add number of timesteps as cost
        sums[1] += dP;
        sums[2] += dP*dP;
        sums[3] += dP*dP*dP;
        sums[4] += dP*dP*dP*dP;
        sums[5] += Pf;
        sums[6] += Pf*Pf;
      }
    }
  }
}

//
// Drift in Heston model
//

void mu_Hestonf(float *x, float h, float ( &m )[2]) {
  m[0] = r * x[0];
  // Standard Euler discretisation of Heston model
  //m[1] = lambda*(sig*sig - x[1]);

  // Improved Euler discretisation of Heston model based on change
  // of variables (removing drift from instantanious variance equation)
  m[1] = ((1.0f-expf(-lambda*h))/h) * (sig*sig-x[1]);
}

void mu_Heston(double *x, double h, double ( &m )[2]) {
  m[0] = r * x[0];
  // Standard Euler discretisation of Heston model
  //m[1] = lambda*(sig*sig - x[1]);

  // Improved Euler discretisation of Heston model based on change
  // of variables (removing drift from instantanious variance equation)
  m[1] = ((1.0-exp(-lambda*h))/h) * (sig*sig-x[1]);
}

//
// Volatility in Heston model
//

void sigdW_Hestonf(float *x, float *dW, float h, float ( &sigdW )[2]) {
  float dW2 = rho*dW[0] + sqrt(1.0f - rho*rho)*dW[1];

  // Standard Euler discretisation of Heston model
  //sigdW[0] = sqrt(fmaxf(0.0f, x[1])) * x[0] * dW[0];
  //sigdW[1] = xsi * sqrt(fmaxf(0.0f, x[1])) * dW2;

  // Improved Euler discretisation of Heston model based on change
  // of variables (removing drift from instantanious variance equation)
  sigdW[0] = sqrt(fmaxf(0.0f, x[1])) * x[0] * dW[0];
  sigdW[1] = expf(-lambda*h) * xsi * sqrt(fmaxf(0.0f, x[1])) * dW2;
}

void sigdW_Heston(double *x, double *dW, double h, double ( &sigdW )[2]) {
  double dW2 = rho*dW[0] + sqrt(1.0 - rho*rho)*dW[1];

  // Standard Euler discretisation of Heston model
  //sigdW[0] = sqrt(fmaxf(0.0f, x[1])) * x[0] * dW[0];
  //sigdW[1] = xsi * sqrt(fmaxf(0.0f, x[1])) * dW2;

  // Improved Euler discretisation of Heston model based on change
  // of variables (removing drift from instantanious variance equation)
  sigdW[0] = sqrt(fmax(0.0, x[1])) * x[0] * dW[0];
  sigdW[1] = exp(-lambda*h) * xsi * sqrt(fmax(0.0, x[1])) * dW2;
}

//
// Normal CDF function
//

float ncff(float x) {
  return 0.5f+0.5f*erff(sqrtf(0.5f)*x);
}

double ncf(double x) {
  return 0.5+0.5*erf(sqrt(0.5)*x);
}
