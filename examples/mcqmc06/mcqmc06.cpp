/**
 * These are similar to the MLMC tests for the MCQMC06 paper
 * using a Milstein discretisation with 2^l timesteps on level l
 *
 * The figures are slightly different due to
 * -- change in MSE split
 * -- change in cost calculation
 * -- different random number generation
 * -- switch to S_0=100
 */

#include "mlmc_test_100.cpp"  // master file for 100 iid tests
#include "mlmc_test.cpp"      // master file for convergence tests

#include <random>           // C++11 random number generators
#include <functional>

//
// global variables
//

int option;         // parameter to specify the financial option
// GBM model parameters
const float r   = 0.05f;      // drift
const float sig = 0.2f;       // local volatility
const float K   = 100.0f;     // strike price
const float B   = 0.85f*K;    // barrier value
const float S0  = K;          // initial value stock price
const float T   = 1.0f;       // time interval

//
// function declarations
//

void mcqmc06_l(int l, int N, double *sums);
// Low-level routine returns sums containing N samples of the
// level l Milstein discretisation of GBM for a given option

float ncff(float x);
// Returns the Normal CDF evaluated at x

//
// RNG: declare generator and output distributions
//

std::default_random_engine rng;
std::normal_distribution<float> normal(0.0f, 1.0f);
std::exponential_distribution<float> exponential(1.0f);

auto next_normal = std::bind(std::ref(normal), std::ref(rng));
auto next_exponential = std::bind(std::ref(exponential), std::ref(rng));

//
// main code
//

int main(int argc, char **argv) {

  int N0 = 200;   // initial samples on each level
  int Lmin = 2;   // minimum refinement level
  int Lmax = 10;  // maximum refinement level

  int   N, L;
  float Eps[11];
  char  filename[32];
  FILE *fp;

//
// loop over different payoffs
//

  for (option = 1; option < 6; option++) {
    rng.seed(1234);
    normal.reset();
    exponential.reset();

    sprintf(filename, "mcqmc06_%d.txt", option);
    fp = fopen(filename, "w");

    if (option == 1) {
      printf("\n ---- option %d: European call ----\n", option);
      N      = 20000;    // samples for convergence tests
      L      = 8;        // levels for convergence tests
      float Eps2[] = { 0.005, 0.01, 0.02, 0.05, 0.1, 0.0 };
      memcpy(Eps, Eps2, sizeof(Eps2));
    }
    else if (option == 2) {
      printf("\n ---- option %d: Asian call ----\n", option);
      N      = 20000;    // samples for convergence tests
      L      = 8;        // levels for convergence tests
      float Eps2[] = { 0.005, 0.01, 0.02, 0.05, 0.1, 0.0 };
      memcpy(Eps, Eps2, sizeof(Eps2));
    }
    else if (option == 3) {
      printf("\n ---- option %d: lookback call ----\n", option);
      N      = 20000;    // samples for convergence tests
      L      = 10;       // levels for convergence tests
      float Eps2[] = { 0.005, 0.01, 0.02, 0.05, 0.1, 0.0 };
      memcpy(Eps, Eps2, sizeof(Eps2));
    }
    else if (option == 4) {
      printf("\n ---- option %d: digital call ----\n", option);
      N      = 200000;   // samples for convergence tests
      L      = 8;        // levels for convergence tests
      float Eps2[] = { 0.01, 0.02, 0.05, 0.1, 0.2, 0.0 };
      memcpy(Eps, Eps2, sizeof(Eps2));
    }
    else if (option == 5) {
      printf("\n ---- option %d: barrier call ----\n", option);
      N      = 200000;   // samples for convergence tests
      L      = 8;        // levels for convergence tests
      float Eps2[] = { 0.005, 0.01, 0.02, 0.05, 0.1, 0.0 };
      memcpy(Eps, Eps2, sizeof(Eps2));
    }

    mlmc_test(mcqmc06_l, N,L, N0,Eps, Lmin,Lmax, fp);

    fclose(fp);

    //
    // print exact analytic value
    //

    float sig2, d1, d2, val, k, d3, d4;

    sig2 = sig*sig;

    if (option == 1) {
      d1  = ( logf(S0/K) + (r+0.5f*sig2)*T) / (sig*sqrtf(T));
      d2  = ( logf(S0/K) + (r-0.5f*sig2)*T) / (sig*sqrtf(T));
      val = S0*ncff(d1) - K*expf(-r*T)*ncff(d2);
    }
    else if (option == 2) {
      val = nanf("");
    }
    else if (option == 3) {
      k   = 0.5f*sig2/r;
      d1  = (r+0.5f*sig2)*T / (sig*sqrtf(T));
      d2  = (r-0.5f*sig2)*T / (sig*sqrtf(T));
      val = S0*( ncff(d1) - ncff(-d1)*k - exp(-r*T)*(ncff(d2) - ncff(d2)*k) );
    }
    else if (option == 4) {
      d2  = ( logf(S0/K) + (r-0.5f*sig2)*T) / (sig*sqrtf(T));
      val = K*expf(-r*T)*ncff(d2);
    }
    else {
      k   = 0.5f*sig2/r;
      if ( K > B ) {
        d1  = ( logf(S0/K) + (r+0.5f*sig2)*T) / (sig*sqrtf(T));
        d2  = ( logf(S0/K) + (r-0.5f*sig2)*T) / (sig*sqrtf(T));
        d3  = ( logf(B/S0) + logf(B/K) + (r+0.5f*sig2)*T) / (sig*sqrtf(T));
        d4  = ( logf(B/S0) + logf(B/K) + (r-0.5f*sig2)*T) / (sig*sqrtf(T));
      }
      else {
        d1  = ( logf(S0/B) + (r+0.5f*sig2)*T) / (sig*sqrtf(T));
        d2  = ( logf(S0/B) + (r-0.5f*sig2)*T) / (sig*sqrtf(T));
        d3  = ( logf(B/S0) + (r+0.5f*sig2)*T) / (sig*sqrtf(T));
        d4  = ( logf(B/S0) + (r-0.5f*sig2)*T) / (sig*sqrtf(T));
      }
      val =  S0*ncff(d1) - K*expf(-r*T)*ncff(d2) \
    - powf(S0/B, 1.0f-1.0f/k)*( (B*B)/(S0)*ncff(d3) - K*expf(-r*T)*ncff(d4) ) ;
    }

    if (isnan(val)) {
      PRINTF2(fp, "\n Exact value unknown \n");
    }
    else {
      PRINTF2(fp, "\n Exact value: %f \n", val);
    }

    //
    // now do 100 independent MLMC calcs
    //

    sprintf(filename, "mcqmc06_%d_100.txt", option);
    fp = fopen(filename, "w");
    mlmc_test_100(mcqmc06_l, val, N0,Eps,Lmin,Lmax, fp);

    fclose(fp);
  }
}


/*-------------------------------------------------------
 *
 * level l estimator
 *
 */


void mcqmc06_l(int l, int N, double *sums) {

  int   nf, nc;
  float hf, hc, X0, Xf, Xc, Af, Ac, Mf, Mc, Bf, Bc,
        Xf0, Xc0, Xc1, vf, vc, dWc, ddW, Pf, Pc, dP;
  float dWf[2], dIf[2], Lf[2];

  // Fix refinement factor to 2
  nf = 1 << l;
  nc = nf/2;

  hf = T / ((float) nf);
  hc = T / ((float) nc);

  for (int k = 0; k < 7; k++) sums[k] = 0.0;

  for (int np = 0; np < N; np++) {
    X0 = K;

    Xf = X0;
    Xc = Xf;

    Af  = 0.5f*hf*Xf;
    Ac  = 0.5f*hc*Xc;

    Mf  = Xf;
    Mc  = Xc;

    Bf  = 1.0f;
    Bc  = 1.0f;

    if (l == 0) {
      dWf[0] = sqrt(hf) * next_normal();
      Lf[0]  = next_exponential();
      dIf[0] = sqrt(hf/12.0f)*hf * next_normal();

      Xf0 = Xf;
      Xf  = Xf + r*Xf*hf + sig*Xf*dWf[0]
               + 0.5f*sig*sig*Xf*(dWf[0]*dWf[0]-hf);
      vf  = sig*Xf0;
      Af  = Af + 0.5f*hf*Xf + vf*dIf[0];
      Mf  = fminf(Mf,
            0.5f*(Xf0+Xf-sqrtf((Xf-Xf0)*(Xf-Xf0)+2.0f*hf*vf*vf*Lf[0])));
      Bf  = Bf*(1.0f-expf(-2.0f*fmaxf(0.0f, (Xf0-B)*(Xf-B)/(hf*vf*vf))));
    }
    else {
      for (int n = 0; n < nc; n++) {
        dWf[0] = sqrt(hf) * next_normal();
        dWf[1] = sqrt(hf) * next_normal();
        Lf[0]  = next_exponential();
        Lf[1]  = next_exponential();
        dIf[0] = sqrt(hf/12.0f)*hf * next_normal();
        dIf[1] = sqrt(hf/12.0f)*hf * next_normal();

        for (int m = 0; m < 2; m++) {
          Xf0 = Xf;
          Xf  = Xf + r*Xf*hf + sig*Xf*dWf[m]
                   + 0.5f*sig*sig*Xf*(dWf[m]*dWf[m]-hf);
          vf  = sig*Xf0;
          Af  = Af + hf*Xf + vf*dIf[m];
          Mf  = fminf(Mf,
                0.5f*(Xf0+Xf-sqrtf((Xf-Xf0)*(Xf-Xf0)+2.0f*hf*vf*vf*Lf[m])));
          Bf  = Bf * (1.0f-expf(-2.0f*fmaxf(0.0f, (Xf0-B)*(Xf-B)/(hf*vf*vf))));
        }

        dWc = dWf[0] + dWf[1];
        ddW = dWf[0] - dWf[1];

        Xc0 = Xc;
        Xc  = Xc + r*Xc*hc + sig*Xc*dWc + 0.5f*sig*sig*Xc*(dWc*dWc-hc);

        vc  = sig*Xc0;
        Ac  = Ac + hc*Xc + vc*(dIf[0]+dIf[1] + 0.25f*hc*ddW);
        Xc1 = 0.5f*(Xc0 + Xc + vc*ddW);
        Mc  = fminf(Mc,
            0.5f*(Xc0+Xc1-sqrtf((Xc1-Xc0)*(Xc1-Xc0)+2.0f*hf*vc*vc*Lf[0])));
        Mc  = fminf(Mc,
            0.5f*(Xc1+Xc -sqrtf((Xc -Xc1)*(Xc -Xc1)+2.0f*hf*vc*vc*Lf[1])));
        Bc  = Bc * (1.0f-expf(-2.0f*fmaxf(0.0f, (Xc0-B)*(Xc1-B)/(hf*vc*vc))));
        Bc  = Bc * (1.0f-expf(-2.0f*fmaxf(0.0f, (Xc1-B)*(Xc -B)/(hf*vc*vc))));
      }
      Af = Af - 0.5f*hf*Xf;
      Ac = Ac - 0.5f*hc*Xc;
    }

    if (option == 1) {
      Pf  = fmaxf(0.0f, Xf-K);
      Pc  = fmaxf(0.0f, Xc-K);
    }
    else if (option == 2) {
      Pf  = fmaxf(0.0f, Af-K);
      Pc  = fmaxf(0.0f, Ac-K);
    }
    else if (option == 3) {
      Pf  = Xf - Mf;
      Pc  = Xc - Mc;
    }
    else if (option == 4) {
      Pf  = K*ncff((Xf0+r*Xf0*hf-K)/(sig*Xf0*sqrt(hf)));
      if (l == 0)
        Pc = Pf;
      else
        Pc = K
           * ncff((Xc0+r*Xc0*hc+sig*Xc0*dWf[0]-K)/(sig*Xc0*sqrt(hf)));
    }
    else if (option == 5) {
      Pf  = Bf*fmaxf(0.0f, Xf-K);
      Pc  = Bc*fmaxf(0.0f, Xc-K);
    }

    dP  = exp(-r*T)*(Pf-Pc);
    Pf  = exp(-r*T)*Pf;

    if (l == 0) dP = Pf;

    sums[0] += nf;     // add number of timesteps as cost
    sums[1] += dP;
    sums[2] += dP*dP;
    sums[3] += dP*dP*dP;
    sums[4] += dP*dP*dP*dP;
    sums[5] += Pf;
    sums[6] += Pf*Pf;
  }
}

//
// Normal CDF function
//

float ncff(float x) {
  return 0.5f+0.5f*erff(sqrtf(0.5f)*x);
}

