%
% execute C++ code
%

% !mcqmc06

%
% use MATLAB routine to plot output
%

addpath('../../../MATLAB/mlmc-core/');

nvert = 3;

data_dir   = './';
figure_dir = './figures/';

% Create figure directory if it doesn't exist
[~,~,~] = mkdir(figure_dir);

for option = 1:5
  filename = ['mcqmc06_' num2str(option)];
  mlmc_plot([data_dir filename], nvert);

  if (nvert==1)
    figure(1)
    print('-deps2c',[figure_dir filename '_a.eps'])
    figure(2)
    print('-deps2c',[figure_dir filename '_b.eps'])
  else
    print('-deps2c',[figure_dir filename '.eps'])
  end

  filename = strcat(filename, '_100');
  mlmc_plot_100([data_dir filename]);
  print('-deps2c',[figure_dir filename '.eps'])
end
