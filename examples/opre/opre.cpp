/**
 * These are similar to the MLMC tests for the original
 * 2008 Operations Research paper, using an Euler-Maruyama
 * discretisation with 4^l timesteps on level l.
 *
 * The differences are:
 * -- the plots do not have the extrapolation results
 * -- the top two plots are log_2 rather than log_4
 * -- the new MLMC driver is a little different
 * -- switch to X_0=100 instead of X_0=1
 */

#include "mlmc_test_100.cpp"  // master file for 100 iid tests
#include "mlmc_test.cpp"      // master file for convergence tests

#include <random>           // C++11 random number generators
#include <functional>
#include <math.h>

//
// global variables
//

int option;  // parameter to specify the financial option

// GBM model parameters
const float r   = 0.05f;      // drift
const float sig = 0.2f;       // local volatility
const float K   = 100.0f;     // strike price & initial value
const float S0  = 100.0f;     // initial value stock price
const float T   = 1.0f;       // time interval
// Heston model additional parameters
const float lambda = 5.0f;    // rate of reversal of instantaneous variance
const float xsi    = 0.25f;   // volatility of volatility
const float V0     = 0.04f;   // initial value instantaneous variance
const float rho    = -0.5f;   // correlation between dW_S and dW_V

//
// function declarations
//

void opre_l(int, int, double *);
// Low-level routine returns sums containing N samples of the
// level l Euler-Maruyama discretisation of underlying model
// for a given option

void mu_Hestonf(float *x, float h, float ( & )[2]);
// Returns the drift in the Heston model
void sigdW_Hestonf(float *x, float *dW, float h, float ( & )[2]);
// Returns the volatility in the Heston model

float ncff(float x);
// Returns the Normal CDF evaluated at x

//
// declare generator and output distributions
//

std::default_random_engine rng;
std::normal_distribution<float> normal(0.0f, 1.0f);

auto next_normal = std::bind(std::ref(normal), std::ref(rng));

//
// main code
//

int main(int argc, char **argv) {

  int N0 = 1000;  // initial samples on each level
  int Lmin = 2;   // minimum refinement level
  int Lmax = 6;   // maximum refinement level

  int   N, L;
  float Eps[11];
  char  filename[32];
  FILE *fp;

//
// loop over different payoffs
//

  for (option = 1; option < 6; option++) {
    rng.seed(1234);
    normal.reset();

    if (option < 5) {
      sprintf(filename, "opre_gbm%d.txt", option);
    }
    else {
      sprintf(filename, "opre_heston.txt");
    }
    fp = fopen(filename, "w");

    if (option == 1) {
      printf("\n ---- European call ----\n");
      N      = 2000000;  // samples for convergence tests
      L      = 5;        // levels for convergence tests
      float Eps2[] = { 0.005, 0.01, 0.02, 0.05, 0.1, 0.0 };
      memcpy(Eps, Eps2, sizeof(Eps2));
    }
    else if (option == 2) {
      printf("\n ---- Asian call ----\n");
      N      = 2000000;  // samples for convergence tests
      L      = 5;        // levels for convergence tests
      float Eps2[] = { 0.005, 0.01, 0.02, 0.05, 0.1, 0.0 };
      memcpy(Eps, Eps2, sizeof(Eps2));
    }
    else if (option == 3) {
      printf("\n ---- lookback call ----\n");
      N      = 2000000;  // samples for convergence tests
      L      = 5;        // levels for convergence tests
      float Eps2[] = { 0.01, 0.02, 0.05, 0.1, 0.2, 0.0 };
      memcpy(Eps, Eps2, sizeof(Eps2));
    }
    else if (option == 4) {
      printf("\n ---- digital call ----\n");
      N      = 2000000;  // samples for convergence tests
      L      = 5;        // levels for convergence tests
      float Eps2[] = { 0.02, 0.05, 0.1, 0.2, 0.5, 0.0 };
      memcpy(Eps, Eps2, sizeof(Eps2));
    }
    else if (option == 5) {
      printf("\n ---- Heston model ----\n");
      N      = 2000000;  // samples for convergence tests
      L      = 5;        // levels for convergence tests
      float Eps2[] = { 0.005, 0.01, 0.02, 0.05, 0.1, 0.0 };
      memcpy(Eps, Eps2, sizeof(Eps2));
    }

    mlmc_test(opre_l, N,L, N0,Eps, Lmin,Lmax, fp);

    fclose(fp);

    //
    // print exact analytic value
    //

    float sig2, d1, d2, val, k;

    sig2 = sig*sig;

    d1  = ( logf(S0/K) + (r+0.5f*sig2)*T) / (sig*sqrtf(T));
    d2  = ( logf(S0/K) + (r-0.5f*sig2)*T) / (sig*sqrtf(T));

    if (option == 1) {
      val = S0*ncff(d1) - K*expf(-r*T)*ncff(d2);
    }
    else if (option == 3) {
      k   = 0.5f*sig2/r;
      d1  = (r+0.5f*sig2)*T / (sig*sqrtf(T));
      d2  = (r-0.5f*sig2)*T / (sig*sqrtf(T));
      val = S0*( ncff(d1) - ncff(-d1)*k - exp(-r*T)*(ncff(d2) - ncff(d2)*k) );
    }
    else if (option == 4) {
      val = K*expf(-r*T)*ncff(d2);
    }
    else {
      val = nanf("");
    }

    if (isnan(val)) {
      PRINTF2(fp, "\n Exact value unknown \n");
    }
    else {
      PRINTF2(fp, "\n Exact value: %f \n", val);
    }

    //
    // now do 100 MLMC calcs
    //

    if (option < 5) {
      sprintf(filename, "opre_gbm%d_100.txt", option);
    }
    else {
      sprintf(filename, "opre_heston_100.txt");
    }
    fp = fopen(filename, "w");
    mlmc_test_100(opre_l, val, N0,Eps,Lmin,Lmax, fp);

    fclose(fp);
  }
}


/*-------------------------------------------------------
 *
 * level l estimator
 *
 */


void opre_l(int l, int N, double *sums) {

  int   nf, nc;
  float hf, hc, Af, Ac, Mf, Mc, Pf, Pc, dP;
  float X0[2], Xf[2], Xc[2], dWc[2], dWf[2];

  int M = 4;  // discretisation refinement parameter

  nf = (int) powf((float) M, (float) l);
  nc = nf/M;

  hf = T / ((float) nf);
  hc = T / ((float) nc);

  for (int k = 0; k < 7; k++) sums[k] = 0.0;

  for (int np = 0; np < N; np++) {
    //
    // GBM model
    //
    if (option < 5) {
      X0[0] = S0;

      Xf[0] = X0[0];
      Xc[0] = Xf[0];

      Af = 0.5f*hf*Xf[0];
      Ac = 0.5f*hc*Xc[0];

      Mf = Xf[0];
      Mc = Xc[0];

      if (l == 0) {
        dWf[0] = sqrtf(hf) * next_normal();

        Xf[0] = Xf[0] + r*Xf[0]*hf + sig*Xf[0]*dWf[0];
        Af    = Af + 0.5f*hf*Xf[0];
        Mf    = fminf(Mf, Xf[0]);
      }
      else {
        for (int n = 0; n < nc; n++) {
          dWc[0] = 0.0f;
          for (int m = 0; m < M; m++) {
            dWf[0]  = sqrtf(hf) * next_normal();
            dWc[0] += dWf[0];

            Xf[0] = Xf[0] + r*Xf[0]*hf + sig*Xf[0]*dWf[0];
            Af    = Af + hf*Xf[0];
            Mf    = fminf(Mf, Xf[0]);
          }

          Xc[0] = Xc[0] + r*Xc[0]*hc + sig*Xc[0]*dWc[0];
          Ac    = Ac + hc*Xc[0];
          Mc    = fminf(Mc, Xc[0]);
        }
        Af = Af - 0.5f*hf*Xf[0];
        Ac = Ac - 0.5f*hc*Xc[0];
      }

      if (option == 1) {
        Pf = fmaxf(0.0f, Xf[0]-K);
        Pc = fmaxf(0.0f, Xc[0]-K);
      }
      else if (option == 2) {
        Pf = fmaxf(0.0f, Af-K);
        Pc = fmaxf(0.0f, Ac-K);
      }
      else if (option == 3) {
        float beta = 0.5826f; /* special factor for offset correction */
        Pf = Xf[0] - Mf*(1-beta*sig*sqrtf(hf));
        Pc = Xc[0] - Mc*(1-beta*sig*sqrtf(hc));
      }
      else if (option == 4) {
        Pf = K * 0.5f*(copysign(1.0f, Xf[0]-K)+1);
        Pc = K * 0.5f*(copysign(1.0f, Xc[0]-K)+1);
      }
    }
    //
    // Heston model
    //
    else {
      float mu[2], sigdW[2];

      X0[0] = S0;
      X0[1] = V0;

      Xf[0] = X0[0];
      Xf[1] = X0[1];
      Xc[0] = Xf[0];
      Xc[1] = Xf[1];

      if (l == 0) {
        dWf[0] = sqrtf(hf) * next_normal();
        dWf[1] = sqrtf(hf) * next_normal();

        mu_Hestonf(Xf, hf, mu);
        sigdW_Hestonf(Xf, dWf, hf, sigdW);

        Xf[0]  = Xf[0] + mu[0]*hf + sigdW[0];
        Xf[1]  = Xf[1] + mu[1]*hf + sigdW[1];
      }
      else {
        for (int n = 0; n < nc; n++) {
          dWc[0]  = 0.0f;
          dWc[1]  = 0.0f;
          for (int m = 0; m < M; m++) {
            dWf[0]  = sqrtf(hf) * next_normal();
            dWf[1]  = sqrtf(hf) * next_normal();
            dWc[0] += dWf[0];
            dWc[1] += dWf[1];

            mu_Hestonf(Xf, hf, mu);
            sigdW_Hestonf(Xf, dWf, hf, sigdW);

            Xf[0]  = Xf[0] + mu[0]*hf + sigdW[0];
            Xf[1]  = Xf[1] + mu[1]*hf + sigdW[1];
          }
          mu_Hestonf(Xc, hc, mu);
          sigdW_Hestonf(Xc, dWc, hc, sigdW);

          Xc[0]  = Xc[0] + mu[0]*hc + sigdW[0];
          Xc[1]  = Xc[1] + mu[1]*hc + sigdW[1];
        }
      }

      Pf  = fmaxf(0.0f, Xf[0]-K);
      Pc  = fmaxf(0.0f, Xc[0]-K);
    }

    dP  = exp(-r*T)*(Pf-Pc);
    Pf  = exp(-r*T)*Pf;

    if (l == 0) dP = Pf;

    sums[0] += nf;     // add number of timesteps as cost
    sums[1] += dP;
    sums[2] += dP*dP;
    sums[3] += dP*dP*dP;
    sums[4] += dP*dP*dP*dP;
    sums[5] += Pf;
    sums[6] += Pf*Pf;
  }
}

//
// Drift in Heston model
//

void mu_Hestonf(float *x, float h, float ( &m )[2]) {
  m[0] = r * x[0];
  // Standard Euler discretisation of Heston model
  //m[1] = lambda*(sig*sig - x[1]);

  // Improved Euler discretisation of Heston model based on change
  // of variables (removing drift from instantanious variance equation)
  m[1] = ((1.0f-expf(-lambda*h))/h) * (sig*sig-x[1]);
}

//
// Volatility in Heston model
//

void sigdW_Hestonf(float *x, float *dW, float h, float ( &sigdW )[2]) {
  float dW2 = rho*dW[0] + sqrtf(1.0f - rho*rho)*dW[1];

  // Standard Euler discretisation of Heston model
  //sigdW[0] = sqrtf(fmaxf(0.0f, x[1])) * x[0] * dW[0];
  //sigdW[1] = xsi * sqrtf(fmaxf(0.0f, x[1])) * dW2;

  // Improved Euler discretisation of Heston model based on change
  // of variables (removing drift from instantanious variance equation)
  sigdW[0] = sqrtf(fmaxf(0.0f, x[1])) * x[0] * dW[0];
  sigdW[1] = expf(-lambda*h) * xsi * sqrtf(fmaxf(0.0f, x[1])) * dW2;
}


//
// Normal CDF function
//

float ncff(float x) {
  return 0.5f+0.5f*erff(sqrtf(0.5f)*x);
}

