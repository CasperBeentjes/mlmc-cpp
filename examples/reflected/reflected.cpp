/**
 *  This tests the use of MLMC for a reflected diffusion
 *  in the interval [0,1] with reflections at either end
 */

#include "mlmc_test_100.cpp"  // master file for 100 iid tests
#include "mlmc_test.cpp"      // master file for convergence tests

#include <random>           // C++11 random number generators
#include <functional>
#include <math.h>

//
// global variables
//

int mass;   // parameter to specify type of particle
int option; // parameter to specify type of drift and volatility

// Model parameters (fixed)
const float X0 =  0.2f;       // initial position
const float U0 = -0.2f;       // initial veloctiy for particle with mass
const float T  =  1.0f;       // time interval
// Model parameters (changing based on option)
float a;   // drift
float b;   // volatility
float bp;  // coefficient for linear variation in volatility

//
// function declarations
//

void massless_l(int l, int N, double *sums);
// Low-level routine returns sums containing N samples of the
// level l Milstein discretisation of a massless particle
void massive_l(int l, int N, double *sums);
// Low-level routine returns sums containing N samples of the
// level l Euler-Maruyama discretisation of a particle with mass

float modulof(float x, float y);
// Returns modulus after division similar to MATLAB
// x - floor(x/y)*y, NOTE: for negative numbers this
// gives different behaviour than fmodf (cmath lib)

//
// declare generator and output distributions
//

std::default_random_engine rng;
std::normal_distribution<float> normal(0.0f, 1.0f);

auto next_normal = std::bind(std::ref(normal), std::ref(rng));

//
// main code
//

int main(int argc, char **argv) {

  int N    = 100000;  // samples for convergence tests
  int L    = 10;      // levels for convergence tests

  float Eps[] = { 0.0002, 0.0005, 0.002, 0.005, 0.0 };
  // target accuracies

  int N0   = 200;  // initial samples on each level
  int Lmin = 2;    // minimum refinement level
  int Lmax = 10;   // maximum refinement level

  char  filename[32];
  FILE *fp;

//
// loop over models
//

  for (mass = 0; mass < 2; mass++) {
    for (option = 1; option < 3; option++) {
      rng.seed(1234);
      normal.reset();

      if (option == 1) {
          a  = 0.0f;
          b  = 0.5f;
          bp = 0.0f;
      }
      else {
          a  = -0.2f;
          b  =  0.5f;
          bp =  0.5f;
      }

      if (mass ==  0) {
        sprintf(filename, "massless_%d.txt", option);
        fp = fopen(filename, "w");
        mlmc_test(massless_l, N,L, N0,Eps,Lmin,Lmax, fp);
        fclose(fp);
      }
      else {
        sprintf(filename, "massive_%d.txt", option);
        fp = fopen(filename, "w");
        mlmc_test(massive_l, N,L, N0,Eps,Lmin,Lmax, fp);
        fclose(fp);
      }

      //
      // now do 100 MLMC calcs in parallel
      //

      float val = nanf("");  // Exact answer not known

      if (mass ==  0) {
        sprintf(filename, "massless_%d_100.txt", option);
        fp = fopen(filename, "w");
        mlmc_test_100(massless_l, val, N0,Eps,Lmin,Lmax, fp);
        fclose(fp);
      }
      else {
        sprintf(filename, "massive_%d_100.txt", option);
        fp = fopen(filename, "w");
        mlmc_test_100(massive_l, val, N0,Eps,Lmin,Lmax, fp);
        fclose(fp);
      }
    }
  }
}

/*--------------------------------------------------------
 *
 * level l estimator for reflected simulation without mass
 *
 * the drift is a, the volatility is b + bp*x
 *
 */


void massless_l(int l, int N, double *sums) {

  int   nf, nc, Sf, Sc;
  float hf, hc, Xf, Xc, dWc, dWf, Pf, Pc, dP;

  int M = 2;  // discretisation refinement parameter

  nf = (int) powf((float) M, (float) l);
  nc = nf/M;

  hf = T / ((float) nf);
  hc = T / ((float) nc);

  for (int k = 0; k < 7; k++) sums[k] = 0.0;

  for (int np = 0; np < N; np++) {

    Xf = X0;
    Xc = Xf;
    Sf = 1;
    Sc = Sf;
    Pf = 0.0f;
    Pc = 0.0f;

    if (l == 0) {
      dWf = sqrt(hf) * next_normal();

      Xf += a*hf + (b + bp*Xf)*Sf*dWf
           + 0.5f*bp*(b + bp*Xf)*(dWf*dWf - hf);

      // modulo shift into [0,2] range
      Xf = modulof(Xf, 2.0f);
      // then reflect if needed
      if (Xf > 1.0f) {
          Xf = 2.0f - Xf;
          Sf = -Sf;
      }

      Pf += hf*Xf;
    }
    else {
      for (int n = 0; n < nc; n++) {
        dWc = 0.0f;
        for (int m = 0; m < M; m++) {
          dWf  = sqrt(hf) * next_normal();
          dWc += dWf;

          Xf += a*hf + (b + bp*Xf)*Sf*dWf
               + 0.5f*bp*(b + bp*Xf)*(dWf*dWf - hf);

          // modulo shift into [0,2] range
          Xf = modulof(Xf, 2.0f);
          // then reflect if needed
          if (Xf > 1.0f) {
              Xf = 2.0f - Xf;
              Sf = -Sf;
          }

          Pf += hf*Xf;
        }

        Xc += a*hc + (b + bp*Xc)*Sc*dWc
             + 0.5f*bp*(b + bp*Xc)*(dWc*dWc - hc);

        // modulo shift into [0,2] range
        Xc = modulof(Xc, 2.0f);
        // then reflect if needed
        if (Xc > 1.0f) {
            Xc = 2.0f - Xc;
            Sc = -Sc;
        }

        Pc += hc*Xc;
      }
    }

    // uncomment these lines to switch to final position
//    Pf = Xf;
//    if (l > 0) Pc = Xc;

    dP  = (Pf-Pc);

    if (l == 0) dP = Pf;

    sums[0] += nf;     // cost defined as number of fine timesteps
    sums[1] += dP;
    sums[2] += dP*dP;
    sums[3] += dP*dP*dP;
    sums[4] += dP*dP*dP*dP;
    sums[5] += Pf;
    sums[6] += Pf*Pf;
  }
}


/*--------------------------------------------------------
 *
 * level l estimator for reflected simulation with mass
 *
 * the drift is a, the volatility is b + bp*x
 *
 */


void massive_l(int l, int N, double *sums) {

  int   nf, nc, Sf, Sc;
  float hf, hc, Xf, Xc, Uf, Uc, dWc, dWf, Pf, Pc, dP;

  int M = 2;  // discretisation refinement parameter

  nf = (int) powf((float) M, (float) l);
  nc = nf/M;

  hf = T / ((float) nf);
  hc = T / ((float) nc);

  for (int k = 0; k < 7; k++) sums[k] = 0.0;

  for (int np = 0; np < N; np++) {

    Xf = X0;
    Xc = Xf;
    Uf = U0;
    Uc = Uf;
    Sf = 1;
    Sc = Sf;
    Pf = 0.0f;
    Pc = 0.0f;

    if (l == 0) {
      dWf = sqrt(hf) * next_normal();

      Xf += Uf*hf;
      Uf += a*hf + (b + bp*Xf)*Sf*dWf;

      // modulo shift into [0,2] range
      Xf = modulof(Xf, 2.0f);
      // then reflect if needed
      if (Xf > 1.0f) {
          Xf = 2.0f - Xf;
          Uf = -Uf;
          Sf = -Sf;
      }

      Pf += hf*Xf;
    }
    else {
      for (int n = 0; n < nc; n++) {
        dWc = 0.0f;
        for (int m = 0; m < M; m++) {
          dWf  = sqrt(hf) * next_normal();
          dWc += dWf;

          Xf += Uf*hf;
          Uf += a*hf + (b + bp*Xf)*Sf*dWf;

          // modulo shift into [0,2] range
          Xf = modulof(Xf, 2.0f);
          // then reflect if needed
          if (Xf > 1.0f) {
              Xf = 2.0f - Xf;
              Uf = -Uf;
              Sf = -Sf;
          }

          Pf += hf*Xf;
        }

        Xc += Uc*hc;
        Uc += a*hc + (b + bp*Xc)*Sc*dWc;

        // modulo shift into [0,2] range
        Xc = modulof(Xc, 2.0f);
        // then reflect if needed
        if (Xc > 1.0f) {
            Xc = 2.0f - Xc;
            Uc = -Uc;
            Sc = -Sc;
        }

        Pc += hc*Xc;
      }
    }

    // uncomment these lines to switch to final position
    Pf = Xf;
    if (l > 0) Pc = Xc;

    dP  = (Pf-Pc);

    if (l == 0) dP = Pf;

    sums[0] += nf;     // cost defined as number of fine timesteps
    sums[1] += dP;
    sums[2] += dP*dP;
    sums[3] += dP*dP*dP;
    sums[4] += dP*dP*dP*dP;
    sums[5] += Pf;
    sums[6] += Pf*Pf;
  }
}

float modulof(float x, float y) {
    return x - floorf(x/y)*y;
}

