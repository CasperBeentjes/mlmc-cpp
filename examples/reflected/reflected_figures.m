%
% execute C++ code
%

% !reflected

%
% use MATLAB routine to plot output
%

addpath('../../../MATLAB/driver/');

nvert = 3;

for mass = 0:1
    for p = 1:2

        if mass==0
            filename = ['massless_' num2str(p)];
        else
            filename = ['massive_' num2str(p)];
        end
        mlmc_plot(filename, nvert);

        if(nvert==1)
            figure(1)
            print('-deps2',[filename 'a.eps'])
            figure(2)
            print('-deps2',[filename 'b.eps'])
        else
            print('-deps2',[filename '.eps'])
        end
        
        filename = [filename '_100'];
        
        mlmc_plot_100(filename);
        print('-deps2',[filename '.eps'])        
    end
end
