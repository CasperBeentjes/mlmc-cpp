#!/usr/bin/env python

#
# First execute C++ code
#

# !reflected

#
# use Python MLMC library to plot output
#

from pymlmc import mlmc_plot, mlmc_plot_100
import matplotlib
import matplotlib.pyplot as plt
import os

if __name__ == "__main__":
    nvert = 3
    error_bars = False

    # Location of data files
    data_dir = './'
    # Location to store figure output
    figure_dir = 'figures/'
    if not os.path.exists(figure_dir):
        os.makedirs(figure_dir)

    for mass in range(0,2):
        for p in range(1,3):

            if mass == 0:
                filename = "massless_" + str(p)
            else:
                filename = "massive_" + str(p)

            mlmc_plot(data_dir + filename, nvert, error_bars)

            if (nvert == 1):
                plt.figure(1)
                plt.savefig(figure_dir + filename + "_a.eps",
                        bbox_inches='tight')

                plt.figure(2)
                plt.savefig(figure_dir + filename + "_b.eps",
                        bbox_inches='tight')

            else:
                plt.savefig(figure_dir + filename + ".eps",
                        bbox_inches='tight')
            plt.close('all')

            filename = filename + '_100'
            mlmc_plot_100(data_dir + filename + '.txt')
            plt.savefig(figure_dir + filename + ".eps",
                    bbox_inches='tight')
            plt.close('all')
